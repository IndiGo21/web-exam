function sortByRate(arr) {
    arr.sort((a, b) => a.rate > b.rate ? -1 : 1);
}

function createAlertDanger(msg) {
    document.getElementById('forAlerts').innerHTML = `<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Ошибка! </strong>${msg}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button></div>`
}

function createAlertSuccess(msg) {
    document.getElementById('forAlerts').innerHTML = `<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Выполнено! </strong>${msg}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button></div>`
}

function sendOption(ar, dist) {
    let district = document.getElementById(dist);
    let opt;
    if (document.getElementById(ar).value == "Не выбрано") { //если выбрали "Не выбрано" то у нас нет записей о районах
        district.innerHTML = 'Не выбрано';
    } else {
        district.innerHTML = '';
        opt = document.createElement('option');
        opt.innerHTML = 'Не выбрано';
        district.append(opt);
        area = document.getElementById(ar).value;
        let districts = district_map.get(area); // переменная districts равна массиву районов из MAP в котором ключ равен округу (в map у нас округ и массив районов)
        districts.forEach(element => { // для каждого элемента массива создаем option с его значением и засовываем в список районов
            opt = document.createElement('option');
            opt.innerHTML = element;
            district.append(opt);
        });
    }
}

function loadInfoModal(number) {
    document.getElementById('infoModalLongTitle').innerText = lastReceivedFilteredData[number].name;
    document.getElementById('nameInfoModal').innerText = lastReceivedFilteredData[number].name;
    document.getElementById('infoNetwork').innerText = lastReceivedFilteredData[number].isNetObject;
    document.getElementById('infoCompanyModal').innerText = lastReceivedFilteredData[number].operatingCompany;
    document.getElementById('infoObjectModal').innerText = lastReceivedFilteredData[number].typeObject;
    document.getElementById('infoAdmArea').innerText = lastReceivedFilteredData[number].admArea;
    document.getElementById('infoDistrict').innerText = lastReceivedFilteredData[number].district;
    document.getElementById('infoAddress').innerText = lastReceivedFilteredData[number].address;
    document.getElementById('infoSeats').innerText = lastReceivedFilteredData[number].seatsCount;
    document.getElementById('infoSocial').innerText = lastReceivedFilteredData[number].socialPrivileges;
    document.getElementById('infoPhone').innerText = lastReceivedFilteredData[number].publicPhone;
}

function loadInfoModalUpdate(number) {
    document.getElementById('nameSend').value = lastReceivedFilteredData[number].name;
    if (lastReceivedFilteredData[number].isNetObject == 1) {
        document.getElementById('networkSend1').checked = true;
    } else {
        document.getElementById('networkSend2').checked = true;
    }
    document.getElementById('companySend').value = lastReceivedFilteredData[number].operatingCompany;
    document.getElementById('typeSend').value = lastReceivedFilteredData[number].typeObject;
    document.getElementById('admAreaSend').value = lastReceivedFilteredData[number].admArea;
    sendOption('admAreaSend', 'districtSend');
    document.getElementById('districtSend').value = lastReceivedFilteredData[number].district;
    document.getElementById('addressSend').value = lastReceivedFilteredData[number].address;
    document.getElementById('placesSend').value = lastReceivedFilteredData[number].seatsCount;
    if (lastReceivedFilteredData[number].socialPrivileges == 1) {
        document.getElementById('socialPrivilegesSend1').checked = true;
    } else {
        document.getElementById('socialPrivilegesSend1').checked = true;
    }
    document.getElementById('phoneSend').value = lastReceivedFilteredData[number].publicPhone;
    document.getElementById('modalFooter').innerHTML = `<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button> <button id = "sendModal"type = "button"class = "btn btn-success" > Новый заказ </button> <button id = "updateModal"type = "button"class = "btn btn-primary" data-id="${number}"> Обновить запись </button>`;
    document.getElementById('sendModal').addEventListener('click', function () {
        let sendInfo = new FormData(document.getElementById('formSend'));
        sendRequest("http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1?api_key=fcdcf462-0f24-4750-9ddf-dfc42c63576d", 'POST', function () {
            console.log(this.response);
            if (this.response.error == "Введены некорректные данные. Ошибка сохранения.") {
                createAlertDanger(this.response.error);
            } else {
                createAlertSuccess('Запись добавлена');
            }
        }, sendInfo);
    })
    document.getElementById('updateModal').addEventListener('click', function () {
        let sendInfo = new FormData(document.getElementById('formSend'));
        let id = lastReceivedFilteredData[event.target.dataset.id].id;
        sendRequest(`http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1/${id}?api_key=fcdcf462-0f24-4750-9ddf-dfc42c63576d`, 'PUT', function () {
            console.log(this.response);
            if (this.response.error == "Пользователь может редактировать только свои записи.") {
                createAlertDanger(this.response.error);
            } else {
                createAlertSuccess('Запись обновлена');
            }
        }, sendInfo);
    })
}

function loadInfoModalSend() {
    document.getElementById('nameSend').value = '';
    document.getElementById('networkSend1').checked = false;
    document.getElementById('networkSend2').checked = false;
    document.getElementById('companySend').value = '';
    document.getElementById('typeSend').value = '';
    document.getElementById('admAreaSend').value = 'Не выбрано';
    sendOption('admAreaSend', 'districtSend');
    document.getElementById('districtSend').value = "";
    document.getElementById('addressSend').value = '';
    document.getElementById('placesSend').value = '';
    document.getElementById('socialPrivilegesSend1').checked = false;
    document.getElementById('socialPrivilegesSend1').checked = false;
    document.getElementById('phoneSend').value = '';
    document.getElementById('modalFooter').innerHTML = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button> <button id = "sendModal"type = "button"class = "btn btn-success" > Новый заказ </button>';
    document.getElementById('sendModal').addEventListener('click', function () {
        let sendInfo = new FormData(document.getElementById('formSend'));
        sendRequest("http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1?api_key=fcdcf462-0f24-4750-9ddf-dfc42c63576d", 'POST', function () {
            console.log(this.response);
            if (this.response.error == "Введены некорректные данные. Ошибка сохранения.") {
                createAlertDanger(this.response.error);
            } else {
                createAlertSuccess('Запись добавлена');
            }
        }, sendInfo);
    })
}

function createLi(page, bl = false) { //
    let li = document.createElement('li');
    li.classList.add('page-item');
    if (bl) {
        li.classList.add('active');
    }
    let btn = document.createElement('button');
    btn.classList.add('page-link');
    btn.classList.add('btn');
    btn.classList.add('btn-sm');
    btn.classList.add('btn-outline-light');
    btn.innerHTML = page;
    li.append(btn);
    return li;
}

function pagination(page) { //создаем клавишы пагинации
    let li;
    let paginationContainer = document.getElementById('pagination');
    paginationContainer.innerHTML = '';

    li = createLi(1); //Первая страница
    li.children[0].innerText = "Первая";
    li.children[0].classList.add("d-none");
    li.children[0].classList.add("d-md-block");
    if (Math.max(Number(page - 2), 1) == 1) {
        li.style.display = 'none';
    }
    paginationContainer.append(li);

    let start = Math.max(Number(page - 2), 1); //Страницы внутри
    let end = Math.min(Number(page) + 2, Number(Math.floor(lastReceivedFilteredData.length / 20)));
    for (let i = start; i <= end; i++) {
        paginationContainer.append(createLi(i, i == page ? true : false));
    }

    li = createLi(Number(Math.floor(lastReceivedFilteredData.length / 20))); //Последняя страница
    li.children[0].innerText = "Последняя";
    li.children[0].classList.add("d-none");
    li.children[0].classList.add("d-md-block");
    if (Number(page) + 2 >= Number(Math.floor(lastReceivedFilteredData.length / 20))) {
        li.style.display = 'none';
    }
    paginationContainer.append(li);
}

function checkNumSeats(seat) {
    let n = Number(document.getElementById(seat).value);
    if (n !== n || n < 0) {
        n = 0;
    } else if (n > 500) {
        n = 500;
    }
    document.getElementById(seat).value = n;
}

function filterRecords(records) { //Фильтрует записи по выбранным фильтрам и сортирует по рейтингу, записывает данные в глобальную переменную
    lastReceivedFilteredData = [];
    let type = document.getElementById('type').value;
    let admArea = document.getElementById('admArea').value;
    let district = document.getElementById('district').value;
    let social = document.getElementById('social').value;
    let maxSeats = document.getElementById('maxSeats').value;
    let minSeats = document.getElementById('minSeats').value;
    let netObject = document.getElementById('netObject').value;
    let newname = document.getElementById('nameFilter').value;
    for (record of records) {
        if (record.name.includes(newname) && ((netObject == 'Да' && record.isNetObject == 1) || (netObject == 'Нет' && record.isNetObject == 0) || (netObject == 'Не выбрано')) && (Number(record.seatsCount) > Number(minSeats) || minSeats == '') && (Number(record.seatsCount) < Number(maxSeats) || maxSeats == "") && ((record.typeObject == type) || (type == "Не выбрано")) && ((record.admArea == admArea) || (admArea == "Не выбрано")) && ((record.district == district) || (district == 'Не выбрано')) && ((Number(record.socialPrivileges) > 0 && social == "есть") || (Number(record.socialPrivileges) == 0 && social == "нет") || social == "не выбрано")) {
            lastReceivedFilteredData.push(record);
        }
    }
    sortByRate(lastReceivedFilteredData);
}

function createRecords() { //Создает элемент в tbody
    let t = document.getElementById('record');
    let row;
    let td;
    t.innerHTML = '';
    for (let i = (page - 1) * 20; i < (lastReceivedFilteredData.length > page * 20 ? page * 20 : lastReceivedFilteredData.length); i++) {
        t.append(createRow(i));
        document.getElementById('r' + i).onclick = function () {
            loadInfoModal(i);
        }
        document.getElementById('d' + i).onclick = function () {
            document.getElementById('deleteInfo').innerText = `Вы уверены, что хотите удалить данные предприятия ${lastReceivedFilteredData[i].name}`;
            document.getElementById('deleteInfoCheck').dataset.id = i;
        }
        document.getElementById('u' + i).onclick = function () {
            loadInfoModalUpdate(i);
        }
    }
}

function createRow(number) { // Создает строку (tr) в таблице по номеру фильтрованного элемента элемента 
    row = document.createElement('tr');
    td = document.createElement('td');
    td.innerHTML = lastReceivedFilteredData[number].name;
    td.classList.add('px-1');
    row.append(td);
    td = document.createElement('td');
    td.innerHTML = lastReceivedFilteredData[number].typeObject;
    td.classList.add('px-1');
    row.append(td);
    td = document.createElement('td');
    td.innerHTML = lastReceivedFilteredData[number].address;
    td.classList.add('px-1');
    row.append(td);
    td = document.createElement('td');
    td.innerHTML = `<i id="r${number}" style="cursor: pointer;" class="fa fa-square-o pr-1" data-toggle="modal" data-target="#infoCompany"></i><i id="d${number}" style="cursor: pointer;" class="fa fa-trash-o px-1" data-toggle="modal" data-target="#deleteModal"></i><i id="u${number}" style="cursor: pointer;" class="fa fa-pencil px-1" data-toggle="modal" data-target="#writeCompany"></i>`;
    td.children[0].setAttribute('data-id', lastReceivedFilteredData[number].id)
    td.classList.add('px-1');
    row.append(td);
    return row;
}

function renderRecords(records) { //фильтрует, создает записи и проставляет пагинацию
    filterRecords(records);
    createRecords();
    pagination(page);
}


function record_path(id) {
    return `/api/data1/${id}`
}

function sendRequest(url, method, onloadHandler, params = '') { // создается xhr и при загрузке выполняется функция
    let xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.responseType = 'json';
    xhr.onload = onloadHandler;
    if (params == '') {
        xhr.send();
    } else {
        xhr.send(params);
    }

}

let host = "http://exam-2020-1-api.std-900.ist.mospolytech.ru";
let records_path = '/api/data1';
let lastReceivedFilteredData = []; //массив отфильтрованных и отсортированных данных
let page = 1; //текущая страница
let record; //текущая запись о заведении
let menu = []; //информация о меню
let sum; //стоимость всех заказов
let rand; //рандомный заказ

window.onload = function () {

    sendRequest("http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1", 'GET', function () { //начальный автоматический запрос
        renderRecords(this.response);
    })

    document.getElementById('downloadDataBtn').onclick = function () { // при нажатии на кнопку Найти у нас происходит GET запрос и делается renderRecords
        page = 1;
        let url = new URL(records_path, host);
        sendRequest(url, 'GET', function () {
            renderRecords(this.response);
        })
    }
    document.getElementById('admArea').onchange = function () { // ставим обработчик ивентов на выбор административного округа
        sendOption('admArea', 'district');
    }

    document.getElementById('admAreaSend').onchange = function () { // ставим обработчик ивентов на выбор административного округа
        sendOption('admAreaSend', 'districtSend');
    }

    document.getElementById('minSeats').addEventListener('input', function () {
        checkNumSeats('minSeats');
    })

    document.getElementById('maxSeats').addEventListener('input', function () {
        checkNumSeats('maxSeats');
    })


    document.getElementById('pagination').onclick = function (event) { //при нажатии на кнопки пагинации у нас просто обрабатывается массив уже отфильтрованных данных
        if (event.target.nodeName != "UL") { // если нажать за пределы кнопок не будет обрабатываться запрос 
            if (event.target.innerText == "Первая") { // если первая страница
                page = 1;
            } else if (event.target.innerText == "Последняя") { // если последняя страница
                page = Number(Math.floor(lastReceivedFilteredData.length / 20));
            } else {
                page = event.target.innerText;
            }
            createRecords();
            pagination(page);
        }
    }

    document.getElementById("loadInfoModalSend").onclick = loadInfoModalSend;

    document.getElementById('deleteInfoCheck').onclick = function (event) {
        let id = lastReceivedFilteredData[event.target.dataset.id].id;
        sendRequest(`http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1/${id}?api_key=fcdcf462-0f24-4750-9ddf-dfc42c63576d`, 'DELETE', function () {
            if (this.response.error == "Пользователь может удалять только свои записи.") {
                createAlertDanger(this.response.error);
            } else {
                createAlertSuccess('Запись успешно удалена');
            }
        })
    }
}