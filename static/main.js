function sortByRate(arr) {
    arr.sort((a, b) => a.rate > b.rate ? -1 : 1);
}

function createLi(page, bl = false) { //
    let li = document.createElement('li');
    li.classList.add('page-item');
    if (bl) {
        li.classList.add('active');
    }
    let btn = document.createElement('button');
    btn.classList.add('page-link');
    btn.classList.add('btn');
    btn.classList.add('btn-sm');
    btn.classList.add('btn-outline-light');
    btn.innerHTML = page;
    li.append(btn);
    return li;
}

function pagination(page) { //создаем клавишы пагинации
    let li;
    let paginationContainer = document.getElementById('pagination');
    paginationContainer.innerHTML = '';

    li = createLi(1); //Первая страница
    li.children[0].innerText = "Первая"
    if (Math.max(Number(page - 2), 1) == 1) {
        li.style.display = 'none';
    }
    paginationContainer.append(li);

    let start = Math.max(Number(page - 2), 1); //Страницы внутри
    let end = Math.min(Number(page) + 2, Number(Math.floor(lastReceivedFilteredData.length / 20)));
    for (let i = start; i <= end; i++) {
        paginationContainer.append(createLi(i, i == page ? true : false));
    }

    li = createLi(Number(Math.floor(lastReceivedFilteredData.length / 20))); //Последняя страница
    li.children[0].innerText = "Последняя"
    if (Number(page) + 2 >= Number(Math.floor(lastReceivedFilteredData.length / 20))) {
        li.style.display = 'none';
    }
    paginationContainer.append(li);
}

function createBody(number) {
    let body = document.createElement('div');
    body.classList.add('card-body');
    let head = document.createElement('h3');
    head.classList.add("card-title");
    head.classList.add("text-center");
    head.innerText = menu[number].name;
    body.append(head);
    let desc = document.createElement('p');
    desc.classList.add("card-text");
    desc.classList.add("text-center");
    desc.innerText = menu[number].descriptiom;
    body.append(desc);
    let price = document.createElement('p');
    price.classList.add("card-text");
    price.classList.add("text-center");
    let set = "set_" + (number + 1);
    price.innerText = record[set] + " руб.";
    price.id = 'p' + number;
    body.append(price);
    let newrow = document.createElement('div');
    newrow.classList.add('row');
    newrow.classList.add('justify-content-center');
    let newcol = document.createElement('div');
    newcol.classList.add("col-auto");
    newcol.classList.add("p-0");
    let newbtn = document.createElement('button');
    newbtn.classList.add('btn');
    newbtn.classList.add('btn-primary');
    newbtn.id = "-" + number;
    newbtn.innerText = '-';
    newcol.append(newbtn);
    newrow.append(newcol);
    newcol = document.createElement('div');
    newcol.classList.add("col-4");
    newcol.classList.add("p-0");
    let newinput = document.createElement('input');
    newinput.classList.add('form-control');
    if (orders == 0) { //если есть заказы то переписываем
        newinput.value = '0';
    } else {
        newinput.value = orders[number];
    }
    newinput.id = 'i' + number;
    newcol.append(newinput);
    newrow.append(newcol);
    newcol = document.createElement('div');
    newcol.classList.add("col-auto");
    newcol.classList.add("p-0");
    newbtn = document.createElement('button');
    newbtn.classList.add('btn');
    newbtn.classList.add('btn-primary');
    newbtn.id = "+" + number;
    newbtn.innerText = '+';
    newcol.append(newbtn);
    newrow.append(newcol);
    body.append(newrow);
    return body;
}


function createCard(number) {
    let card = document.createElement('div');
    let img = document.createElement('img');
    img.src = menu[number].photo;
    img.classList.add('card-img-top');
    card.append(img);
    body = createBody(number);
    card.append(body);
    return card;
}

function sumeval() {
    sum = 0;
    for (let i = 0; i < 10; i++) {
        let priceForProduct = document.getElementById('p' + i).innerText.substr(0, document.getElementById('p' + i).innerText.length - 5);
        sum = sum + Number(document.getElementById('i' + i).value) * Number(priceForProduct);
    }
    document.getElementById('sum').innerText = "Итого: " + sum;
}

function checkNum(i) {
    let n = Number(document.getElementById('i' + i).value);
    if (n !== n || n < 0) {
        n = 0;
    } else if (n > 50) {
        n = 50;
    }
    orders[i] = n;
    document.getElementById('i' + i).value = n;
}

function renderModalDescription() {
    document.getElementById('modalName').innerText = record.name;
    document.getElementById('modalAdmArea').innerText = record.admArea;
    document.getElementById('modalDistrict').innerText = record.district;
    document.getElementById('modalAddress').innerText = record.address;
    document.getElementById('modalRate').innerText = record.rate;
}

function renderModalOption(i) {
    let modalOption = document.getElementById('modalOption');
    let newrow = document.createElement('div');
    newrow.classList.add('row');
    newrow.classList.add('align-items-center');
    newrow.classList.add('border');
    newrow.classList.add('justify-content-center');
    let newcol = document.createElement('div');
    newcol.classList.add('col-sm-4');
    newcol.classList.add('col-10');
    newcol.classList.add('py-2');
    let newimg = document.createElement('img');
    newimg.src = menu[i].photo;
    newimg.classList.add('img-fluid');
    newcol.append(newimg);
    newrow.append(newcol);
    newcol = document.createElement('div');
    newcol.classList.add('col-sm-2');
    newcol.classList.add('col-3');
    newcol.classList.add('py-2');
    newcol.innerText = menu[i].name;
    newrow.append(newcol);
    newcol = document.createElement('div');
    newcol.classList.add('offset-1');
    newcol.classList.add('col-sm-2');
    newcol.classList.add('col-3');
    if (rand - 1 == i) {
        newcol.innerText = (Number(document.getElementById('i' + i).value) + 1) + "x" + document.getElementById('p' + i).innerText + " -" + document.getElementById('p' + i).innerText;
    } else {
        newcol.innerText = document.getElementById('i' + i).value + "x" + document.getElementById('p' + i).innerText;
    }
    newrow.append(newcol);
    newcol = document.createElement('div');
    newcol.classList.add('col-sm-3');
    newcol.classList.add('col-4');
    newcol.classList.add('justify-content-end');
    newcol.classList.add('d-flex');
    newcol.innerText = Number(document.getElementById('i' + i).value) * Number(document.getElementById('p' + i).innerText.substr(0, document.getElementById('p' + i).innerText.length - 5));
    newrow.append(newcol);
    modalOption.append(newrow);
}

function modalOption() {
    document.getElementById('modalOption').innerHTML = '';
    for (let i = 0; i < 10; i++) {
        if (Number(document.getElementById('i' + i).value) > 0) {
            renderModalOption(i);
        }
    }
    if (rand > 0 && Number(document.getElementById('i' + (rand - 1)).value) == 0) {
        renderModalOption(rand - 1);
    }
}

function renderMenu() { // рендерится меню и к меню приписываются
    if (document.getElementById('menuSection').classList[0] = 'd-none') {
        document.getElementById('menuSection').classList.remove('d-none');
    }
    let menuRecords = document.getElementById('menu');
    menuRecords.innerHTML = '';
    let col;
    let card;
    for (let i = 0; i < 10; i++) {
        card = createCard(i);
        card.classList.add('card');
        col = document.createElement('div')
        col.classList.add('col-md-6');
        col.classList.add('card-group');
        col.append(card);
        menuRecords.append(col);
    }
    sumeval();
    for (let i = 0; i < 10; i++) {
        document.getElementById('+' + i).onclick = function () {
            document.getElementById('i' + i).value++;
            checkNum(i);
            modalOption();
            sumeval();
        }
        document.getElementById('-' + i).onclick = function () {
            document.getElementById('i' + i).value--;
            checkNum(i);
            modalOption();
            sumeval();
        }
        document.getElementById('i' + i).addEventListener('input', function () {
            checkNum(i);
            modalOption();
            sumeval();
        })
    }
}

function filterRecords(records) { //Фильтрует записи по выбранным фильтрам и сортирует по рейтингу, записывает данные в глобальную переменную
    lastReceivedFilteredData = [];
    let type = document.getElementById('type').value;
    let admArea = document.getElementById('admArea').value;
    let district = document.getElementById('district').value;
    let social = document.getElementById('social').value;
    for (record of records) {
        if (((record.typeObject == type) || (type == "Не выбрано")) && ((record.admArea == admArea) || (admArea == "Не выбрано")) && ((record.district == district) || (district == 'Не выбрано')) && ((Number(record.socialPrivileges) > 0 && social == "есть") || (Number(record.socialPrivileges) == 0 && social == "нет") || social == "не выбрано")) {
            lastReceivedFilteredData.push(record);
        }
    }
    sortByRate(lastReceivedFilteredData);
}

function createRecords() { //Создает элемент в tbody
    let t = document.getElementById('record');
    let row;
    let td;
    t.innerHTML = '';
    for (let i = (page - 1) * 20; i < (lastReceivedFilteredData.length > page * 20 ? page * 20 : lastReceivedFilteredData.length); i++) {
        t.append(createRow(i));
    }
}

function createRow(number) { // Создает строку (tr) в таблице по номеру фильтрованного элемента элемента 
    row = document.createElement('tr');
    td = document.createElement('td');
    td.innerHTML = lastReceivedFilteredData[number].name;
    td.classList.add('px-1');
    row.append(td);
    td = document.createElement('td');
    td.innerHTML = lastReceivedFilteredData[number].typeObject;
    td.classList.add('px-1');
    row.append(td);
    td = document.createElement('td');
    td.innerHTML = lastReceivedFilteredData[number].address;
    td.classList.add('px-1');
    row.append(td);
    td = document.createElement('td');
    td.innerHTML = "<button type=\"submit\" class=\"btn btn-sm btn-outline-light\">Выбрать</button>";
    td.children[0].setAttribute('data-id', lastReceivedFilteredData[number].id)
    td.classList.add('px-1');
    row.append(td);
    return row;
}

function renderRecords(records) { //фильтрует, создает записи и проставляет пагинацию
    filterRecords(records);
    createRecords();
    pagination(page);
}


function record_path(id) {
    return `/api/data1/${id}`
}

function sendRequest(url, method, onloadHandler) { // создается xhr и при загрузке выполняется функция
    let xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.responseType = 'json';
    xhr.onload = onloadHandler;
    xhr.send();
}

let host = "http://exam-2020-1-api.std-900.ist.mospolytech.ru";
let records_path = '/api/data1';
let lastReceivedFilteredData = []; //массив отфильтрованных и отсортированных данных
let page = 1; //текущая страница
let record; //текущая запись о заведении
let menu = []; //информация о меню
let sum; //стоимость всех заказов
let rand; //рандомный заказ
let orders = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //запомнил сколько было заказов у элемента меню

window.onload = function () {

    sendRequest("http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1", 'GET', function () { //начальный автоматический запрос
        renderRecords(this.response);
    })

    document.getElementById('downloadDataBtn').onclick = function () { // при нажатии на кнопку Найти у нас происходит GET запрос и делается renderRecords
        page = 1;
        let url = new URL(records_path, host);
        sendRequest(url, 'GET', function () {
            renderRecords(this.response);
        })
    }
    document.getElementById('admArea').onchange = function () { // ставим обработчик ивентов на выбор административного округа
        let district = document.getElementById('district');
        let opt;
        if (document.getElementById('admArea').value == "Не выбрано") { //если выбрали "Не выбрано" то у нас нет записей о районах
            district.innerHTML = 'Не выбрано';
        } else {
            district.innerHTML = '';
            opt = document.createElement('option');
            opt.innerHTML = 'Не выбрано';
            district.append(opt);
            area = document.getElementById('admArea').value;
            let districts = district_map.get(area); // переменная districts равна массиву районов из MAP в котором ключ равен округу (в map у нас округ и массив районов)
            districts.forEach(element => { // для каждого элемента массива создаем option с его значением и засовываем в список районов
                opt = document.createElement('option');
                opt.innerHTML = element;
                district.append(opt);
            });
        }
    }
    document.getElementById('record').onclick = function (event) { // ставит ивент на нажатие клавиши у определенного заведения
        if (event.target.nodeName == "BUTTON") { // если нажать за пределы кнопок не будет обрабатываться запрос 
            let id = event.target.dataset.id;
            let url = new URL(record_path(id), host);
            sendRequest(url, 'GET', function () { //создает запрос с конкретным ID
                record = this.response; //ответ записывается в глобальную переменную
                renderModalDescription(); //рендерится описание в модальном окне
                document.getElementById('optionalOptions').classList.remove('d-none'); //Видно дополнительные опции
                sendRequest("menu.json", 'GET', function () {
                    menu = this.response;
                    renderMenu(); //рендерит меню
                })
            })
        }
    }
    document.getElementById('check1').onchange = function (event) { //ставит имент на бесконтактную доставку
        if (event.target.checked) {
            document.getElementById('modalCheck1').classList.remove('d-none')
        } else {
            document.getElementById('modalCheck1').classList.add('d-none')
        }
    }
    document.getElementById('check2').onchange = function (event) { //ставит ивент на Подарок
        if (event.target.checked) { //если чекбокс отмечен
            rand = Math.floor(Math.random() * 10); //рандомит число
            document.getElementById('modalCheck2').classList.remove('d-none'); //в модальном окне показывает Опцию подарок
            document.getElementById('presentType').innerText = "1 " + menu[rand - 1].name + " бесплатно"; //вставляет описание к подарку ( что бесплатно)
            modalOption(); // Запускает обработку модального окна
        } else {
            document.getElementById('modalCheck2').classList.add('d-none'); //убирает в модальном окне опцию подарок
            document.getElementById('presentType').innerText = "";
            rand = -1; //убирает рандом
            modalOption(); // Запускает обработку модального окна
        }
    }
    document.getElementById('pagination').onclick = function (event) { //при нажатии на кнопки пагинации у нас просто обрабатывается массив уже отфильтрованных данных
        if (event.target.nodeName != "UL") { // если нажать за пределы кнопок не будет обрабатываться запрос 
            if (event.target.innerText == "Первая") { // если первая страница
                page = 1;
            } else if (event.target.innerText == "Последняя") { // если последняя страница
                page = Number(Math.floor(lastReceivedFilteredData.length / 20));
            } else {
                page = event.target.innerText;
            }
            createRecords();
            pagination(page);
        }
    }
}